SHELL := /bin/bash
# Install bluetooth-autoconnect

DESTDIR=debian/bluetooth-autoconnect

install-core:
	install -dm755 $(DESTDIR)/etc/systemd/system/
	install -dm755 $(DESTDIR)/usr/bin/
#	install -dm755 $(DESTDIR)/usr/lib/systemd/system-sleep/
	install -Dpm 0755 bluetooth-autoconnect $(DESTDIR)/usr/bin/bluetooth-autoconnect
	install -Dpm 0644 bluetooth-autoconnect.service $(DESTDIR)/etc/systemd/system/bluetooth-autoconnect.service
#	install -Dpm 0755 bluetooth-suspend $(DESTDIR)/usr/lib/systemd/system-sleep/bluetooth-suspend

install: install-core

uninstall:
	rm -f $(DESTDIR)$(DESTDIR)/usr/bin/bluetooth-autoconnect
	rm -f $(DESTDIR)/etc/systemd/system/bluetooth-autoconnect.service
#	rm -f $(DESTDIR)/usr/lib/systemd/system-sleep/bluetooth-suspend